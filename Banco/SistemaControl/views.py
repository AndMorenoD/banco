from django.shortcuts import render
from .models import *
from django.views.generic.edit import UpdateView, CreateView, DeleteView
from django.views.generic import ListView, DetailView, TemplateView
from django.urls import reverse_lazy 
from SistemaControl import forms


# Create your views here.

def verSaldo(request):
    personas = Persona.objects.all()
    context = {'personas_list': personas}
    return render(request, 'verSaldo.html', context)

def historial(request):
    retiros = Retiro.objects.all()
    context = {'retiros_list': retiros}
    return render(request, 'historial.html', context)
    

class CrearRetiro(CreateView):
    model = Retiro 
    form_class = forms.RetiroForm
    template_name = 'Retirar.html'
    success_url = reverse_lazy('historial')

               


    