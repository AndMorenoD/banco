from django import forms
from .models import *


class RetiroForm(forms.ModelForm):
    
    class Meta:
        model = Retiro
        fields= '__all__'
        widgets = {
            
            'usuario' : forms.Select(attrs={'class':'selectpicker', 'name':'usuario'}),
            'cantidad' : forms.TextInput(attrs={'class':'form-control', 'placeholder':'Para retiros mayores a 10.000.000 presentarse en las oficinas personalmente.'  , 'name':'max_length', 'maxLength':'7' }),
        }

        


        
        