from SistemaControl import views
from django.urls import path

urlpatterns = [
 
    path('RetiroSaldo/', views.CrearRetiro.as_view(), name = 'retiro'),
    path('SC/Saldos/', views.verSaldo, name = 'saldo'),
    path('SC/HistorialRetiros/', views.historial, name = 'historial'),
    
]