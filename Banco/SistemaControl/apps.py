from django.apps import AppConfig


class SistemacontrolConfig(AppConfig):
    name = 'SistemaControl'
