# Generated by Django 2.1.2 on 2018-10-28 22:26

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('SistemaControl', '0008_retiro'),
    ]

    operations = [
        migrations.AlterField(
            model_name='retiro',
            name='cantidad',
            field=models.CharField(max_length=7),
        ),
    ]
